import json
from collections import namedtuple
import os
import random
import re
import requests
import ast
from requests.exceptions import ConnectionError
from time import time, sleep
from datetime import datetime

BOT_VERSION = "0.1"

NOTIFICATION_TYPE_MENTION = 1
NOTIFICATION_TYPE_REPLY = 2
NOTIFICATION_TYPE_PM = 6


class BotCore(object):
    """
    An extensible Discourse bot.
    """
    
    Login = None
    Password = None
    InitCallback = None
    BotAdmin = None
    AboutExtension = None
    BaseUrl = None
    
    MentionCallback = None
    PrivateMessageCallback = None
    ReplyCallback = None

    Log = None
    TestMode = False
    CleverBot = False

    MinTrustLevel = 2
    IgnoredUsers = []
    IgnoredTopics = []
    UserCache = []
    LurkMode = True
    Help = ["#===== Bot Private Message Commands =====",
            "",
            "**about**: shows bot info.",
            "**help**: shows this help screen.",
            ""]
    AdminHelp = ["============ Admin Commands ============",
                 "",
                 "**ignore topic [topic_id]**: (PM) Adds topic to ignore list so the bot will not respond to summons there.",
                 "**stop ignoring topic [topic_id]**: (PM) Removes topic from ignore list.",
                 "**ignore user [username]**: (PM) Adds username to ignore list, bot will not respond to summons by that user.",
                 "**stop ignoring user [username]**: (PM) Removes username from ignore list.",
                 "**show ignore list**: (PM) Shows list of ignored topics and users.",
                 "**set trust level [number]**: (PM) Changes minimum trust level required to interact with the bot. Only available to the bot owner or forum admins/moderators.",
                 "**lurk mode on**: (PM) Puts bot in Lurk Mode. Bot will only respond to PM's, not public summons.",
                 "**lurk mode off**: (PM) Disables Lurk Mode.",
                 "**cleverbot on**: (PM) Turns on CleverBot reply mode.",
                 "**cleverbot off**: (PM) Turns off CleverBot reply mode.",
                 "**test mode on**: (PM) Turns on test mode (tips aren't sent).",
                 "**test mode off**: (PM) Turns off test mode (tips are sent)."]
            
    _currentNotificationType = None

    class WorseThanFailure(Exception):
        pass

    Mention = namedtuple('Mention', ['username', 'topic_id', 'post_number', 'notification_type'])
    Post = namedtuple('Post', ['number', 'id', 'can_like', 'message', 'poster', 'poster_id'])
    CachedUser = namedtuple('CachedUser', ['name', 'json', 'timestamp'])
        
    def __init__(self):
        self._session = requests.Session()
        self._session.headers['X-Requested-With'] = "XMLHttpRequest"
        self._client_id = self._get_client_id()
        self._bus_registrations = {}
        self._bus_callbacks = {}

    def run(self):
        if self.BotAdmin is None or self.BotAdmin == "":
            message = "BotAdmin must be set!"
            self.Log.exception(message)
            raise self.WorseThanFailure(message)
        if self.Login is None or self.Password is None or self.Login == "" or self.Password == "":
            message = "Bot login details not set!"
            self.Log.exception(message)
            raise self.WorseThanFailure(message)
        # Get the CSRF token
        res = self._get("/session/csrf", _=int(time() * 1000))
        if res is False:
            message = "Could not get token"
            self.Log.exception(message)
            raise self.WorseThanFailure(message)
        self._session.headers['X-CSRF-Token'] = res[u'csrf']

        # Login
        res = self._post("/session", login=self.Login, password=self.Password)
        if res is False:
            message = "Failed to login"
            self.Log.exception(message)
            raise self.WorseThanFailure(message)
        if u'error' in res:
            message = res[u'error'].encode('utf8')
            self.Log.exception(message)
            raise self.WorseThanFailure(message)

        my_uid = res[u'user'][u'id']

        self._bus_registrations["/notification/%d" % my_uid] = -1
        self._bus_callbacks["/notification/%d" % my_uid] = self._notify_cb

        self._session.headers['X-SILENCE-LOGGER'] = "true"
        
        # Load persistent ignore list from disk.
        if os.path.isfile("./%s-ignore-users.txt" % self.Login):
            with open("./%s-ignore-users.txt" % self.Login, 'r') as ignore_file:
                for line in ignore_file:
                    self.IgnoredUsers.append(line.strip())
                    
        # Load persistent ignore list from disk.
        if os.path.isfile("./%s-ignore-topics.txt" % self.Login):
            with open("./%s-ignore-topics.txt" % self.Login, 'r') as topic_file:
                for line in topic_file:
                    self.IgnoredTopics.append(int(line.strip()))
                    
        if os.path.isfile("./%s-trust.txt" % self.Login):
            with open("./%s-trust.txt" % self.Login, 'r') as trust_file:
                self.MinTrustLevel = int(trust_file.read())
        
        if self.InitCallback:
            self.InitCallback(self)
        
        if self._handle_notifications() is False:
                self.Log.error("Unable to get mentions")

        self.Log.info("Entering main loop")
        while True:
            data = self._post(
                    "/message-bus/%s/poll" % self._client_id,
                    **self._bus_registrations
                )
            if data is False:
                self.Log.warning("unable to contact message-bus")
                sleep(15)
                continue
            # api not returning json always. catch that here
            lines = data.split('\n')
            try:
                data = json.loads(lines[0])
            except ValueError as e:
                self.Log.warning("got no json in response: {}".format(e.message))
                continue

            for message in data:
                if "channel" not in message:
                    self.Log.warning("no channel found: {}".format(message))
                    break
                channel = message[u'channel']
                if channel in self._bus_registrations:
                    message_id = message[u'message_id']
                    self._bus_registrations[channel] = message_id
                    if self._bus_callbacks[channel](message[u'data'], channel) is False:
                        self.Log.warning("Failed to get messages fo om%s" % channel)
                if channel == u"/__status":
                    for key, value in message[u'data'].iteritems():
                        if key in self._bus_registrations:
                            self._bus_registrations[key] = value
            sleep(15)

    def _notify_cb(self, message, channel):

        count = message[u'unread_private_messages'] + message[u'unread_notifications']
        if count > 0:
            #self.Log.info("Scanning new messages for %s" % channel)
            if self._handle_notifications() is False:
                self.Log.error("Error while handling notifications for %s" % channel)
                return False
        #else:
            #self.Log.info("No new messages from %s" % channel)
        return True

    def _handle_notifications(self):
        mentions = self._get_mentions()
        if mentions is False:
            self.LOG.warning("_get_mentions() returned False")
            return False
        for mention in mentions:

            if self._mark_as_read(mention.topic_id, mention.post_number) is False:
                self.Log.warning("mark_as_read() returned False")
                return False
            
            raw_post_text = self._get_text("/raw/%d/%d" % (mention.topic_id, mention.post_number))
            if raw_post_text is False:
                self.Log.warning("_get_text returned False")
                return False
            user = self._get_user_from_cache(mention.username)
            if user is False:
                self.Log.warning("_get_user_from_cache() returned False")
                return False
            # Ignore summon if user is ignored or user does not have the required trust level.
            if user.json["user"]["trust_level"] < self.MinTrustLevel \
                    or mention.username.lower() in self.IgnoredUsers:
                # Extra check, even if user is ignored or below trust level, allow commands if this is a PM and 
                # the user is a moderator, admin, or the bot owner.
                if not(mention.notification_type == NOTIFICATION_TYPE_PM 
                       and (user.json["user"]["admin"] == "true" 
                            or user.json["user"]["moderator"] == "true" 
                            or mention.username.lower() == self.BotAdmin.lower())):
                    sleep(5)
                    continue
                
            if mention.topic_id in self.IgnoredTopics:
                sleep(5)
                continue
            
            call_call_back = True
            
            # Cache here so extensions don't have to pass notification type back into the reply method.
            self._currentNotificationType = mention.notification_type
            
            # Parse to see if this is a special command handled by the core bot code.
            if mention.notification_type == NOTIFICATION_TYPE_PM:
                
                m = re.match(r"^ignore topic ([0-9]+)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    topic_id = int(m.groups()[0])
                    self.Log.info("ignore topic %d called by %s" % (topic_id, mention.username))
                    if topic_id not in self.IgnoredTopics:
                        self.IgnoredTopics.append(topic_id)
                        message = "Added topic %s/t/topic/%d to ignore list." % (self.BaseUrl, topic_id)
                        self.Log.info(message)
                        message = self.append_unique(message, mention)
                        self.reply_to(mention.topic_id, mention.post_number, message)
                        call_call_back = False
                        continue_command_parsing = False
                        m = None
                        self._save_ignore()
                    
                m = re.match(r"^stop ignoring topic ([0-9]+)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    topic_id = int(m.groups()[0])
                    self.Log.info("stop ignoring topic %d called by %s" % (topic_id, mention.username))
                    if topic_id in self.IgnoredTopics:
                        self.IgnoredTopics.remove(topic_id)
                        message = "Removed topic %s/t/topic/%d from ignore list." % (self.BaseUrl, topic_id)
                        self.Log.info(message)
                        message = self.append_unique(message, mention)
                        self.reply_to(mention.topic_id, mention.post_number, message)
                        call_call_back = False
                        m = None
                        self._save_ignore()
                    
                m = re.match(r"^ignore user (\w+)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    username = m.groups()[0].lower()
                    self.Log.info("ignore user %s called by %s" % (username, mention.username))
                    if username not in self.IgnoredUsers:
                        if username.lower() == self.BotAdmin.lower():
                            message = "@%s is the bot admin and cannot be added to the ignore list." % username
                        else:
                            self.IgnoredUsers.append(username)
                            message = "Added @%s to ignore list." % username
                        self.Log.info(message)
                        message = self.append_unique(message, mention)
                        self.reply_to(mention.topic_id, mention.post_number, message)
                        call_call_back = False
                        m = None
                        self._save_ignore()
                    
                m = re.match(r"^stop ignoring user (\w+)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    username = m.groups()[0].lower()
                    self.Log.info("stop ignoring user %s called by %s" % (username, mention.username))
                    if username in self.IgnoredUsers:
                        self.IgnoredUsers.remove(username)
                        message = "Removed @%s from ignore list." % username
                        self.Log.info(message)
                        message = self.append_unique(message, mention)
                        self.reply_to(mention.topic_id, mention.post_number, message)
                        call_call_back = False
                        m = None
                        self._save_ignore()
                    
                m = re.match(r"^show ignore list[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    self.Log.info("show ignore list called by %s" % mention.username)
                    message = "**Ignored Users**\n\n"
                    for u in self.IgnoredUsers:
                        message += "- @" + u + "\n"
                    message += "\n**Ignored Topics**\n\n"
                    for t in self.IgnoredTopics:
                        message += "- %s/t/topic/%d\n" % (self.BaseUrl, t)
                    self.Log.info(message)
                    message = self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None
                    
                m = re.match(r"^about[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    self.Log.info("about called by %s" % mention.username)
                    message = "BotCore version %s, by @mott555\n" % BOT_VERSION
                    if self.AboutExtension:
                        message += self.AboutExtension + "\n"
                    message += "This bot is owned by @%s.\n" % self.BotAdmin
                    message += "\n"
                    if self.LurkMode:
                        message += "Bot is in lurk mode, I will respond to PM's but not public posts.\n"
                    else:
                        message += "Bot is in public mode, I will respond to any summons I have access to.\n"
                    message += "Minimum Trust Level: %d\n" % self.MinTrustLevel
                    self.Log.info(message)
                    message = self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None
                    
                m = re.match(r"^help[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    self.Log.info("help called by %s" % mention.username)
                    message = "\n\n**Supported Commands:**\n\n"
                    for h in self.Help:
                        message += "- %s\n" % h
                    self.Log.info(message)
                    message = self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None

                m = re.match(r"^admin help[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    self.Log.info("help called by %s" % mention.username)
                    if mention.username.lower() != self.BotAdmin.lower():
                        continue
                    else:
                        message = "\n\n**Supported Admin Commands:**\n\n"
                    for h in self.AdminHelp:
                        message += "- %s\n" % h
                    message = self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None

                m = re.match(r"^lurk mode (on|off)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    mode = m.groups()[0]
                    self.Log.info("lurk mode %s called by %s" % (mode, mention.username))
                    message = None
                    if mode == "on":
                        self.LurkMode = True
                        message = "Lurk Mode enabled."
                    else:
                        self.LurkMode = False
                        message = "Lurk Mode disabled."
                    self.Log.info(message)
                    message = self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None

                m = re.match(r"^cleverbot (on|off)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    mode = m.groups()[0]
                    self.Log.info("cleverbot %s called by %s" % (mode, mention.username))
                    message = None
                    if mode == "on":
                        self.CleverBot = True
                        message = "CleverBot enabled."
                    else:
                        self.LurkMode = False
                        message = "CleverBot disabled."
                    self.Log.info(message)
                    message = self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None

                m = re.match(r"^test mode (on|off)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    mode = m.groups()[0]
                    self.Log.info("test mode %s called by %s" % (mode, mention.username))
                    message = None
                    if mode == "on":
                        self.TestMode = True
                        message = "Test Mode enabled."
                    else:
                        self.TestMode = False
                        message = "Test Mode disabled."
                    self.Log.info(message)
                    message = self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None
                    
                m = re.match(r"^set trust level ([0-4]+)[.]*", raw_post_text, re.IGNORECASE | re.DOTALL)
                if m:
                    trust = int(m.groups()[0])
                    self.Log.info("set trust level %d called by %s" % (trust, mention.username))
                    message = None
                    if mention.username.lower() == self.BotAdmin.lower() or user.json["user"]["admin"] == "true" or user.json["user"]["moderator"] == "true":
                        self.MinTrustLevel = trust
                        message = "Minimum trust level changed to %d." % trust
                        with open("./%s-trust.txt" % self.Login, 'w') as trust_file:
                            trust_file.write(str(trust))
                    else:
                        message = "You do not have permission to change that setting."
                    self.Log.info(message)
                    self.append_unique(message, mention)
                    self.reply_to(mention.topic_id, mention.post_number, message)
                    call_call_back = False
                    m = None
            
            if call_call_back:
                if mention.notification_type == NOTIFICATION_TYPE_PM and self.PrivateMessageCallback:
                    self.PrivateMessageCallback(self, mention, raw_post_text, user.json)
                if mention.notification_type == NOTIFICATION_TYPE_REPLY and self.ReplyCallback and not self.LurkMode:
                    self.ReplyCallback(self, mention, raw_post_text, user.json)
                if mention.notification_type == NOTIFICATION_TYPE_MENTION and self.MentionCallback and not self.LurkMode:
                    self.MentionCallback(self, mention, raw_post_text, user.json)

            sleep(5)
        return True
            
    def _save_ignore(self):
        with open("./%s-ignore-users.txt" % self.Login, 'w') as users_file:
            for user in self.IgnoredUsers:
                users_file.write("%s\n" % user)
        with open("./%s-ignore-topics.txt" % self.Login, 'w') as topics_file:
            for topic in self.IgnoredTopics:
                topics_file.write("%d\n" % topic)
    
    @staticmethod        
    def append_unique(message, mention):
        return "%s<!-- t%d p%d -->" % (message, mention.topic_id, mention.post_number)

    def _get_user_from_cache(self, username):
        cached_user = None
        for u in self.UserCache:
            if u.name == username:
                cached_user = u
        if cached_user is None or (datetime.utcnow() - cached_user.timestamp).total_seconds() > (60 * 30):
            if cached_user in self.UserCache:
                self.UserCache.remove(cached_user)
            user_details = self._get("/users/%s.json" % username, _=int(time() * 1000))
            if user_details is False:
                return False
            cached_user = self.CachedUser(name=username, json=user_details, timestamp=datetime.utcnow())
            self.UserCache.append(cached_user)
        return cached_user

    def reply_to(self, topic_id, post_number, raw_message):
        # No idea what happens if we mix these up
        archetype = 'private_message' if self._currentNotificationType == NOTIFICATION_TYPE_PM else 'regular'

        return self._post("/posts", raw=raw_message, topic_id=topic_id,
                          reply_to_post_number=post_number,
                          archetype=archetype)

    def send_pm(self, raw_message, to_user):
        user_id = None
        topic_id = None
        pages = []
        page_num = 1
        pms = self.get_pms()
        pages.append(pms)
        while page_num < 10000:
            pms = self.get_pms(page_num)
            page_num += 1
            if 'users' not in pms:
                break
            pages.append(pms)
        for page in pages:
            users = page['users']
            for user in users:
                if user['username'] == to_user:
                    user_id = user['id']
            if user_id is None:
                continue
            topics = page['topic_list']['topics']
            for topic in topics:
                posters = topic['posters']
                for poster in posters:
                    if poster['user_id'] == user_id:
                        topic_id = topic['id']
                if topic_id is None:
                    participants = topic['participants']
                    for participant in participants:
                        if participant['user_id'] == user_id:
                            topic_id = topic['id']
        if topic_id is None:
            self.Log.warning("PM sending failed as topic_id not found for %s" % to_user)
            return False
        self.Log.info("sending PM to %s" % to_user)
        return self._post("/posts", raw=raw_message, topic_id=topic_id, archetype="private_message")

    def _mark_as_read(self, topic_id, post_number):
        # Send fake timings
        # I hate special chars in POST keys
        kwargs = {
            'topic_id': topic_id,
            'topic_time': 400,  # m-secs passed on topic (I think)
            'timings[%d]' % post_number: 400  # m-secs passed on post (same)
        }

        return self._post("/topics/timings", **kwargs)

    def _get_mentions(self):
        watched_types = [NOTIFICATION_TYPE_MENTION, NOTIFICATION_TYPE_REPLY, NOTIFICATION_TYPE_PM]

        notifications = self._get("/notifications", _=int(time() * 1000))
        if notifications is False:
            yield False
        for notification in notifications['notifications']:
            if notification['notification_type'] in watched_types and notification['read'] is False:
                data = notification[u'data']
                yield self.Mention(username=data[u'original_username'],
                                   topic_id=notification[u'topic_id'],
                                   post_number=notification[u'post_number'],
                                   notification_type=notification[u'notification_type'])

    def search(self, term):
        return self._get("/search", term=term)

    def get_posts(self, topic_id, post_number):
        return self._get("/t/%s/%s.json?include_raw=1" % (topic_id, post_number))

    def grant_badge(self, username, badge_id):
        return self._post('/user_badges', username=username, badge_id=badge_id)

    def get_pms(self, page=None):
        if page is None:
            return self._get('/topics/private-messages/%s.json' % self.Login)
        return self._get('/topics/private-messages/%s.json' % self.Login, page=page)

    @staticmethod
    def _get_client_id():
        def _replace(letter):
            val = random.randrange(0, 16)
            if letter == "x":
                val = (3 & val) | 8
            return "%x" % val

        return re.sub('[xy]', _replace, "xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx")

    def _get(self, url, **kwargs):
        fail_count = 0
        while fail_count <= 15:
            r = self._session.get(self.BaseUrl + url, params=kwargs)
            if r.status_code != requests.codes.ok:
                self.Log.warning("GET request to %s returned %d %s" % (r.url, r.status_code, r.reason))
                fail_count += 1
                sleep(2)
            else:
                break
        if fail_count == 15:
            return False
        return r.json()

    def _post(self, url, **kwargs):
        fail_count = 0
        while fail_count <= 15:
            try:
                r = self._session.post(self.BaseUrl + url, data=kwargs)
            except ConnectionError as e:
                self.Log.warning("POST request suffered exception: %s" % e.message)
                fail_count += 1
                sleep(2)
                continue
            if r.status_code != requests.codes.ok:
                self.Log.warning("POST request to %s returned %d %s" % (r.url, r.status_code, r.reason))
                self.Log.debug("data = %s" % kwargs)
                fail_count += 1
                if r.status_code == 422:
                    message = u",".join(r.json()[u'errors'])
                    self.Log.exception(message)
                    raise self.WorseThanFailure(message)
                if r.status_code == 429:
                    sleep(5)
                else:
                    sleep(2)
            else:
                break
        if fail_count == 15:
            return False
        if r.headers['Content-type'].startswith('application/json'):
            return r.json()
        return r.content
        
    def _get_text(self, url, **kwargs):
        fail_count = 0
        while fail_count <= 15:
            r = self._session.get(self.BaseUrl + url, params=kwargs)
            if r.status_code != requests.codes.ok:
                self.Log.warning("GET request to %s returned %d %s" % (r.url, r.status_code, r.reason))
                fail_count += 1
                sleep(2)
            else:
                break
        if fail_count == 15:
            return False
        return r.text
