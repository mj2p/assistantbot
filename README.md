#Assistant Bot

##NuBits information, utilities and tipping for Discourse forums
###Developed for [the Nubits forum](https://discuss.nubits.com)
####Based on [BotCore by mott555](https://github.com/mott555/BotCore)
-------

##About

Assistant bot is a python script that monitors a Discourse forum for mentions of it's user name or private messages.  
It is set to respond in certain ways to certain commands.  
A list of the commands is below.


##Commands  

###Private message commands
Assistant bot will only respond to these commands through private messages.  
The private message body needs to have the command as the first word

| Command | Action |
| ------------- | ------------- |
| **about** | Shows bot info. |
| **help** | Shows the help text. |
| | |
| **motion hash [motion]** | Hash a motion into the preferred format. |
| **custodian hash [address] [amount] [text]** | Hash a Custodian Proposal into the preferred format. |
| | |
| **register** | Register for tipping. |
| **info** | Get your current balance and deposit address. |
| **history** | Get the transaction and tipping history of your tipping account. |
| **withdraw [address]** | Withdraw your entire balance from the tipping account to [address]. |
| **keywords** | Show a list of the keywords which can be used in place of an amount when tipping. |


###Mention commands
Assistant bot will only respond to these commands via a mention of the bots username (on discuss.nubits.com, the username is 'assistant')
The command can appear anywhere in a forum post but must follow the format specified below.

| Command | Action |
| ------------- | ------------- |
| **@[user_name] liquidity** | Get the current liquidity information |
| **@[user_name] block count** | Get the current block count |
| **@[user_name] block hash [index]** | Get the hash for the [index] block |
| **@[user_name] custodian votes** | Get the latest Custodian Votes |
| **@[user_name] custodian vote [address]** | Get the vote details for the Custodian address [address] |
| **@[user_name] motion votes** | Get the latest Motion Votes |
| **@[user_name] motion vote [motion_hash]** | Get the vote details for the motion with hash [motion_hash] |
| **@[user_name] park rates** | Get the latest Park Rate values |
| **@[user_name] park rate votes** | Get the latest Park Rate Votes |
| **@[user_name] verify** | Scan the thread for motions and custodial proposals and verify that their hashes are unchanged |
| **@[user_name] qrcode [address]** | Create a QR Code of the valid address. |
| **@[user_name] tip [amount] @[user]** | Send a tip of [amount] NBT to @[user]. [amount] can be a key word. Send a PM to the bot with **keywords** as the first word for a list. |
  

##License

Copyright (c) 2015 woolly_sammoth

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
