import os

__author__ = 'woolly_sammoth'

import re
from BotCore import BotCore
from cleverbot import Cleverbot
from collections import namedtuple
import logging
import pm_commands
import tip_commands
import mention_commands
import utils
import json

from rpc import RPC
nud = RPC()


log = logging.getLogger('Assistant')
log.setLevel(logging.INFO)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='bot.log',
                    filemode='w')
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
log.addHandler(ch)


def init_call_back(bot):
    try:
        nud.get_liquidity_info()
    except:
        log.exception("no connection with nud")
        raise bot.WorseThanFailure("no connection with nud")

    log.info("Bot initialized")


def pm_call_back(bot, mention, rawPostText, user):
    log.info("New PM from %s" % mention.username)

    #hash commands

    motion_hash = re.match(r'^motion hash(.+)', rawPostText, re.IGNORECASE | re.DOTALL)
    if motion_hash:
        log.info("Motion hash requested")
        pm_commands.motion_hash(bot, mention, motion_hash)
        return

    custodian_hash = re.match(r'^custodian hash(.+)', rawPostText, re.IGNORECASE | re.DOTALL)
    if custodian_hash:
        log.info("Custodian hash requested")
        data = custodian_hash.groups()[0].split()
        address = data[0].strip()
        valid = nud.validate_address(address)
        if valid is False or valid['isvalid'] is False:
            log.error("didn't recognise address %s" % address)
            reply = "Hi @%s\n\n" \
                    "I'm afraid your custodial hash failed as %s is not a valid NBT address.\n" \
                    "Please try again." % (mention.username, address)
            bot.reply_to(mention.topic_id, mention.post_number, reply)
            return
        check_amount = data[1].strip()
        if utils.check_amount(check_amount) is False:
            log.error("didn't recognise amount %s" % check_amount)
            reply = "Hi @%s\n\n" \
                    "I'm afraid your custodial hash failed as %s is not a valid amount.\n" \
                    "Please try again." % (mention.username, check_amount)
            bot.reply_to(mention.topic_id, mention.post_number, reply)
            return
        amount = check_amount

        proposal = custodian_hash.groups()[0].replace(address, '', 1).replace(check_amount, '', 1)

        pm_commands.custodian_hash(bot, mention, address, amount, proposal)
        return

    #tip commands

    register = re.match(r'^register(.*)', rawPostText, re.IGNORECASE | re.DOTALL)
    if register:
        log.info("%s wants to register" % mention.username)
        tip_commands.register(bot, mention)
        return

    info = re.match(r'^info(.*)', rawPostText, re.IGNORECASE | re.DOTALL)
    if info:
        log.info("%s is asking for their info" % mention.username)
        tip_commands.info(bot, mention)
        return

    history = re.match(r'^history(.*)', rawPostText, re.IGNORECASE | re.DOTALL)
    if history:
        log.info("%s is asking for their history" % mention.username)
        tip_commands.history(bot, mention)
        return

    withdraw = re.match(r'^withdraw(.*)', rawPostText, re.IGNORECASE | re.DOTALL)
    if withdraw:
        log.info("%s is asking to withdraw their funds" % mention.username)
        tip_commands.withdraw(bot, mention, withdraw)
        return

    keywords = re.match(r'^keywords(.*)', rawPostText, re.IGNORECASE | re.DOTALL)
    if keywords:
        log.info("%s is asking to see the keywords" % mention.username)
        tip_commands.keywords(bot, mention)
        return

    log.warning("Didn't recognise command %s" % rawPostText)
    bad_request = "Hi @%s\n\nI didn't recognise your command\n\n" % mention.username
    bad_request += "Reply with 'help' to see a list of available commands"
    bot.reply_to(mention.topic_id, mention.post_number, bot.append_unique(bad_request, mention))


def reply_call_back(bot, mention, rawPostText, user):
    log.info("New reply from %s" % mention.username)
    if bot.CleverBot is True:
        log.info("respond as CleverBot")
        respond_as_cleverbot(bot, mention, rawPostText, user)
    else:
        post_number = mention.post_number
        username = None
        raw_post_text = None
        while post_number is not None:
            posts = utils.get_posts(bot, mention.topic_id, post_number)
            post = posts.pop()
            while post['post_number'] != post_number:
                post = posts.pop()
            post_number = post['reply_to_post_number']
            username = post['username']
            raw_post_text = post['raw']
        mention_tuple = namedtuple('Mention', ['username', 'topic_id', 'post_number', 'notification_type'])
        new_mention = mention_tuple(username=username, topic_id=mention.topic_id, post_number=post_number, notification_type=1)
        log.info("original request from %s" % username)
        mention_call_back(bot, new_mention, raw_post_text, username)


def respond_as_cleverbot(bot, mention, question, user):
    # Get Cleverbot instance for this topic.
    if mention.topic_id not in bot.conversations:
        bot.conversations[mention.topic_id] = Cleverbot()
    cleverbot = bot.conversations[mention.topic_id]

    # Capitalize first letter of user post.
    if len(question) > 1:
        question = question[0].upper() + question[1:]
    else:
        question = question.capitalize()

    # Submit to Cleverbot API.
    message = cleverbot.ask(question)
    if message == "":
        return
    log.info("question - %s" % question)
    log.info("reply - %s" % message)
    # Prepent original question as a quote.
    message = u'[quote="%s, post:%d, topic:%d"]%s[/quote]' % (mention.username, mention.post_number, mention.topic_id, question) + message
    bot.reply_to(mention.topic_id, mention.post_number, message)


def mention_call_back(bot, mention, rawPostText, user):
    """
    This is where the tipping happens
    """
    log.info("New mention from %s" % mention.username)

    #we want the mention to be the first element of the text.
    #if this first regex matches, we use the output to put the mention first
    first = re.match(r'(.+)@' + bot.Login + '(.+)', rawPostText, re.IGNORECASE | re.DOTALL)

    if first:
        rawPostText = '@' + bot.Login + " " + first.groups()[1]

    action = re.match(r'^@' + bot.Login + '(.+)', rawPostText, re.IGNORECASE | re.DOTALL)

    if action:
        param = action.groups()[0].split()

        if param[0].lower() == "liquidity":
            log.info("liquidity info requested")
            mention_commands.liquidity(bot, mention)
            return

        if param[0].lower() == "block" and param[1].lower() == "count":
            log.info("get block count requested")
            mention_commands.get_block_count(bot, mention)
            return

        if param[0].lower() == "block" and param[1].lower() == "hash":
            log.info("get block hash requested for index %s requested" % param[2])
            mention_commands.get_block_hash(bot, mention, param[2])
            return

        if param[0].lower() == "custodian" and param[1].lower() == "votes":
            log.info("get custodian votes requested")
            mention_commands.get_custodian_votes(bot, mention)
            return

        if param[0].lower() == "custodian" and param[1].lower() == "vote":
            log.info("get custodian vote info for %s requested" % param[2])
            mention_commands.get_custodian_votes(bot, mention, param[2])
            return

        if param[0].lower() == "difficulty":
            log.info("get difficulty requested")
            mention_commands.get_difficulty(bot, mention)
            return

        if param[0].lower() == "motion" and param[1].lower() == "votes":
            log.info("get motion votes requested")
            mention_commands.get_motions(bot, mention)
            return

        if param[0].lower() == "motion" and param[1].lower() == "vote":
            log.info("get motion vote info for %s requested" % param[2])
            mention_commands.get_motions(bot, mention, param[2])
            return

        if param[0].lower() == "park" and param[1].lower() == "rates":
            log.info("get park rates requested")
            mention_commands.get_park_rates(bot, mention)
            return

        if param[0].lower() == "park" and param[1].lower() == "rate" and param[2].lower() == "votes":
            log.info("get park votes requested")
            mention_commands.get_park_votes(bot, mention)
            return

        if param[0].lower() == "tip":
            log.info("tip requested")
            if len(param) > 2:
                to_user = param[2]
            else:
                to_user = None
            tip_commands.tip(bot, mention, param[1], to_user)
            return

        if param[0].lower() == "verify":
            log.info("hash verification requested")
            mention_commands.verify_hashes(bot, mention)
            return

        if param[0].lower() == "qrcode":
            log.info("qrcode requested for %s" % param[1])
            mention_commands.qr_code(bot, mention, param[1])
            return

        log.warning("Didn't recognise command %s" % rawPostText)
        bad_request = "Hi @%s\n\nI didn't recognise your command\n\n" % mention.username
        bad_request += "Send me a PM with the first word 'help' to see a list of available commands"
        if bot.send_pm(bad_request, mention.username) is False:
            bot.reply_to(mention.topic_id, mention.post_number, bot.append_unique(bad_request, mention))

    else:
        bang_text = "Something went bang back here!"
        bot.reply_to(mention.topic_id, mention.post_number, bot.append_unique(bang_text, mention))




if __name__ == "__main__":
    login = None
    password = None
    admin = None
    with open(os.path.join(os.getcwd(), 'conf.json')) as conf_file:
        conf = json.loads(conf_file.read())
        login = conf['discourse_user']
        password = conf['discourse_password']
        admin = conf['discourse_bot_admin']
    if login is None or password is None:
        raise Exception("Failed to load discourse user details")

    try:
        bot = BotCore()
        bot.Log = log

        bot.TestMode = False

        # BotAdmin must be set to your Discourse username to tell the bot who owns it.
        bot.BotAdmin = admin

        # Minimum trust level to interact with the bot. Defaults to 2 if not set. If a user below
        # this trust level tries to interact with the bot, the callbacks will not be called, unless
        # said user is the bot owner, forum admin, or forum moderator.
        # BotCore will cache this in a local file, if present that setting will override this one.
        # bot.MinTrustLevel = 2

        # Discourse login details for the bots account. Login value is also used for local
        # cache file names (ignore list).
        bot.Login = login
        bot.Password = password

        # URL to the Discourse instance, defaults to "http://what.thedailywtf.com" if not set.
        # Changing this is not recommended.
        bot.BaseUrl = "https://discuss.nubits.com"

        # Adds extra information to the bots about screen.
        bot.AboutExtension = "Assistant bot. Helpful about the place"

        # If LurkMode is true, the bot will only respond to PM's, ignoring mentions and replies.
        # It will still consume notifications.
        # This defaults to true. It's a good idea to start in LurkMode and later disable LurkMode by
        # issuing a PM command to the bot when you're ready to make it act publicly.
        bot.LurkMode = False

        # Hook up callbacks for the notifications you are interested in.
        bot.PrivateMessageCallback = pm_call_back
        bot.ReplyCallback = reply_call_back
        bot.MentionCallback = mention_call_back

        # You can append additional items to the bots help screen. Note you must still
        # implement the command in one of the callback methods. You should also note what
        # methods the command supports (PM, reply, mention).
        bot.Help.append("##===== Formatting Commands =====")
        bot.Help.append("")
        bot.Help.append("**motion hash [motion]**: Hash a motion into the preferred format.")
        bot.Help.append("**custodian hash [address] [amount] [text]**: Hash a Custodian Proposal into the preferred format.")
        bot.Help.append("")
        bot.Help.append("##===== Tip Private Message Commands =====")
        bot.Help.append("")
        bot.Help.append("**register**: Register for tipping.")
        bot.Help.append("**info**: Get your current balance and deposit address")
        bot.Help.append("**history**: Get the transaction and tipping history of your tipping account")
        bot.Help.append("**withdraw [address]**: Withdraw your entire balance from the tipping account to [address]")
        bot.Help.append("**keywords**: Show a list of the keywords which can be used in place of an amount when tipping")
        bot.Help.append("")
        bot.Help.append("#===== Bot Mention Commands =====")
        bot.Help.append("(these can appear anywhere in a forum post but the format must be as below)")
        bot.Help.append("")
        bot.Help.append("**@%s liquidity**: Get the current liquidity information" % login)
        bot.Help.append("**@%s block count**: Get the current block count" % login)
        bot.Help.append("**@%s block hash [index]**: Get the hash for the [index] block" % login)
        bot.Help.append("**@%s custodian votes**: Get the latest Custodian Votes" % login)
        bot.Help.append("**@%s custodian vote [address]**: Get the vote details for the Custodian address [address]" % login)
        bot.Help.append("**@%s motion votes**: Get the latest Motion Votes" % login)
        bot.Help.append("**@%s motion vote [motion_hash]**: Get the vote details for the motion with hash [motion_hash]" % login)
        bot.Help.append("**@%s park rates**: Get the latest Park Rate values" % login)
        bot.Help.append("**@%s park rate votes**: Get the latest Park Rate Votes" % login)
        bot.Help.append("**@%s verify**: Scan the thread for motions and custodial proposals and verify that their hashes are unchanged" % login)
        bot.Help.append("**@%s qrcode [address]**: Create a QR Code of the valid NBT address." % login)
        bot.Help.append("")
        bot.Help.append("**@%s tip [amount] @[user]**: Send a tip of [amount] NBT to @[user]." % login)
        bot.Help.append("[amount] can be a key word. Send a PM to the bot with 'keywords' as the first word for a list.")

        # Hook up an initialize callback if needed. This is called after the bot logs in
        # but before it responds to notifications.
        bot.InitCallback = init_call_back

        # Dictionary to store cleverbot conversation history, one per topic.
        bot.conversations = {}

        # Start the bot!
        bot.run()

    except Exception as e:
        message = 'Something went bang\n\n%s' % e.message
        log.exception(message)
        utils.notify(message)
