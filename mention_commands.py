__author__ = 'woolly_sammoth'

import hashlib
import json
import re
import qrcode
import utils

from rpc import RPC
nud = RPC()


def liquidity(bot, mention):
    liquid = nud.get_liquidity_info()
    if liquid is False:
        reply = utils.nud_error(bot, mention, "liquidity")
    else:
        bot.Log.info("Sell : {0} | Buy : {1}".format(liquid['total']['sell'],
                                                     liquid['total']['buy']))
        total = liquid['total']['buy'] + liquid['total']['sell'] 
        reply = "Hi @{0}\n\n" \
                "The current total liquidity in the Nu network is:\n\n" \
                "Bid: {1} NBT ({9}%)\n" \
                "Ask: {2} NBT ({10}%)\n\n\n" \
                "It is broken down in the following manner:\n\n" \
                "```\n\n" \
                "Tier 1:\n" \
                "    Bid: {3} NBT\n" \
                "    Ask: {4} NBT\n\n" \
                "Tier 2:\n" \
                "    Bid: {5} NBT\n" \
                "    Ask: {6} NBT\n\n" \
                "Tier 3:\n" \
                "    Bid: {7} NBT\n" \
                "    Ask: {8} NBT\n\n" \
                "```".format(mention.username,
                                          liquid['total']['buy'],
                                          liquid['total']['sell'],
                                          liquid['tier']['1']['buy'],
                                          liquid['tier']['1']['sell'],
                                          liquid['tier']['2']['buy'],
                                          liquid['tier']['2']['sell'],
                                          liquid['tier']['3']['buy'],
                                          liquid['tier']['3']['sell'],
                                          round((float(liquid['total']['buy']) / float(total)) * 100, 2),
                                          round((float(liquid['total']['sell']) / float(total)) * 100, 2))
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def get_block_count(bot, mention):
    count = nud.get_block_count()
    if count is False:
        reply = utils.nud_error(bot, mention, "block count")
    else:
        bot.Log.info("Block Count : %d" % count)
        reply = "Hi @%s\n\n" \
                "The current count of the highest block on the longest chain is %d" % (mention.username, count)
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def get_block_hash(bot, mention, index):
    try:
        index = int(index)
        block_hash = nud.get_block_hash(index)
        if block_hash is False:
            reply = utils.nud_error(bot, mention, "block hash")
        else:
            bot.Log.info("Block Hash : %d" % block_hash)
            reply = "Hi @%s\n\n" \
                    "The hash of block index %d is:\n" \
                    "%s" % (mention.username, index, block_hash)
    except ValueError:
        reply = "Hi @%s\n\n" \
                "You didn't specify a valid block index so I couldn't fetch the block hash" % mention.username
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def get_custodian_votes(bot, mention, vote=None):
    votes = nud.get_custodian_votes()
    if votes is False:
        reply = utils.nud_error(bot, mention, "custodian vote")
    else:
        elected_custodians = nud.get_elected_custodians()
        if elected_custodians is False:
            reply = utils.nud_error(bot, mention, "get elected custodians")
        else:
            output = ""
            total_blocks = 0
            total_sharedays = 0
            count = 1
            result = None
            if vote is not None:
                reply = "Hi @%s\n\n" \
                        "Here are the details for the Custodian Vote on %s:\n\n" % (mention.username, vote)
            else:
                reply = "Hi @%s\n\n" \
                        "Here are the latest Custodian Votes:\n\n" % mention.username
            for address in votes:
                elected = False
                for custodian in elected_custodians:
                    if address == custodian['address']:
                        elected = True
                if elected is True:
                    continue
                if address == "total":
                    total = votes[address]
                    total_blocks = total['blocks']
                    total_sharedays = total['sharedays']
                    continue
                if vote is not None and address != vote:
                    continue
                output += "\n----------\n"
                detail = votes[address]
                #result = utils.format_search(bot.search(address), "custodian")
                result = False
                if result is False:
                    output += "##%s\n" % address
                else:
                    result = dict(result)
                    url = "%s/t/%s" % (bot.BaseUrl, result['slug'])
                    output += "##[%s][%d]\n" % (address, count)
                    output += "[%d]: %s\n" % (count, url)
                    output += ">###%s\n" % result['title']
                    count += 1
                for amount in detail:
                    scores = dict(detail[amount])
                    output += "###%s NBT.\n" % amount
                    output += "**Blocks**:  %s  (``%f%%``)\n" % (scores['blocks'], scores['block_percentage'])
                    output += "**Share Days**:  %s  (``%f%%``)\n\n" % (scores['sharedays'], scores['shareday_percentage'])
            if output == "":
                bot.Log.info("Invalid Address : %s" % vote)
                reply = "Hi @%s\n\n" \
                        "I couldn't find the details for the Custodian Vote on %s:\n\n" % (mention.username, vote)
            else:
                output += "\n----------\n"
                if vote is not None and result is not None:
                    bot.Log.info("Block %% : %f | Shareday %% : %f" % (scores['block_percentage'], scores['shareday_percentage']))
                    #output += "\n<p>%s</blockquote></p>\n<p><a href=\"%s\"><strong>Read More</strong></a></p>" % (result['cooked'][0:500], url)
                else:
                    bot.Log.info("Total Blocks : %s | Total Share Days : %s" % (total_blocks, total_sharedays))
                    output += "\n####Total Blocks = %s\n####Total Share Days = %s" % (total_blocks, total_sharedays)
                reply += output
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def get_difficulty(bot, mention):
    difficulty = nud.get_difficulty()
    if difficulty is False:
        reply = utils.nud_error(bot, mention, "difficulty")
    else:
        bot.Log.info("Difficulty: %d" % difficulty)
        reply = "Hi @%s\n\n" \
                "The current proof of stake difficulty on the Nu network is %s" % (mention.username, difficulty['proof-of-stake'])
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def get_motions(bot, mention, motion_hash=None):
    motions = nud.get_motions()
    if motions is False:
        reply = utils.nud_error(bot, mention, "motions")
    else:
        output = ""
        count = 1
        result = None
        if motion_hash is not None:
            reply = "Hi @%s\n\n" \
                    "Here are the details for the Motion Vote on %s:\n\n" % (mention.username, motion_hash)
        else:
            reply = "Hi @%s\n\n" \
                    "Here are the latest Motion Votes:\n\n" % mention.username
        for motion in motions:
            if motion_hash is not None and motion != motion_hash:
                continue
            output += "\n----------\n"
            scores = dict(motions[motion])
            #result = utils.format_search(bot.search(motion), "motion")
            result = False
            if result is False:
                output += "##%s\n" % motion
            else:
                result = dict(result)
                url = "%s/t/%s" % (bot.BaseUrl, result['slug'])
                output += "##[%s][%d]\n" % (motion, count)
                output += "[%d]: %s\n" % (count, url)
                output += ">###%s\n" % result['title']
                count += 1
            output += "**Blocks**:  %s  (``%f%%``)\n" % (scores['blocks'], scores['block_percentage'])
            output += "**Share Days**:  %s  (``%f%%``)\n\n" % (scores['sharedays'], scores['shareday_percentage'])
        if output == "":
            bot.Log.info("Invalid Hash: %s" % motions)
            reply = "Hi @%s\n\n" \
                    "I couldn't find the details for the Mustodian Vote on %s:\n\n" % (mention.username, motion_hash)
        else:
            output += "\n----------\n"
            if motion_hash is not None and result is not None:
                bot.Log.info("Block %% : %f | Shareday %% : %f" % (scores['block_percentage'], scores['shareday_percentage']))
                #output += "\n<p>%s</blockquote></p></div><br /><p><a href=\"%s\"><strong>Read More</strong></a></p>" % (result['cooked'][0:500], url)
            reply += output
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def get_park_rates(bot, mention):
    rates = nud.get_park_rates()
    if rates is False:
        reply = utils.nud_error(bot, mention, "park rates")
    else:
        bot.Log.info(rates)
        reply = "Hi @%s\n\n" \
                "The current park rates in the Nu network are:\n\n" % mention.username
        for time, rate in rates.iteritems():
            blocks = time.split(" ")[0]
            apr = float(rate) * 100 / float(int(blocks) * 60 / (365.25 * 24 * 3600))
            reply += "**%s**: %f (%.2f%% APR)\n" % (time, rate, apr)
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def get_park_votes(bot, mention):
    # votes = nud.get_park_votes()
    votes = False
    if votes is False:
        reply = utils.nud_error(bot, mention, "park votes")
    else:
        bot.Log.info(votes)
        reply = "Hi @%s\n\n" \
                "The current park rate votes in the Nu network are:\n\n" % mention.username
        output = ""
        for currency in votes:
            output += "NBT" if currency == "B" else "unknown"
            data = dict(votes[currency])
            for datum in sorted(data):
                duration = data[datum]
                output += "\n\n----------\n\n"
                output += "**%s** (``%d Blocks``)\n\n" % (duration['estimated_duration'], duration['blocks'])
                duration_votes = list(duration['votes'])
                for vote in duration_votes:
                    output += "APR: %d\t|\tShare days: %d(``%d%%``)\t|\tAccumulated: %d%%\n" % (vote['annual_percentage'], vote['sharedays'], vote['shareday_percentage'], vote['accumulated_percentage'])
    reply += output
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def verify_hashes(bot, mention):
    data = []
    post_number = mention.post_number
    topic_id = mention.topic_id

    posts = utils.get_posts(bot, topic_id, post_number)

    while post_number != 1:
        if len(posts) == 0:
            posts = utils.get_posts(bot, topic_id, post_number)
        post = posts.pop()
        test_post_number = post['post_number']
        if test_post_number >= post_number:
            continue
        post_number = post['post_number']
        post_text = post['raw']
        if '<custodianhash>' in post_text:
            bot.Log.info("found custodial hash")
            original_hash = re.match(r'(.+)<calculatedcustodialhash>(.+)', post_text, re.IGNORECASE | re.DOTALL)
            if original_hash is None:
                continue
            original_hash = re.match(r'(.+)</calculatedcustodialhash>(.+)', original_hash.groups()[1], re.IGNORECASE | re.DOTALL)
            if original_hash is None:
                continue
            original_hash = original_hash.groups()[0]

            proposal_text_check = re.match(r'(.+)<custodianhash>(.+)', post_text, re.IGNORECASE | re.DOTALL)
            if proposal_text_check is None:
                continue
            proposal_text_check = re.match(r'(.+)</custodianhash>(.+)', proposal_text_check.groups()[1], re.IGNORECASE | re.DOTALL)
            if proposal_text_check is None:
                continue
            proposal_text_check = proposal_text_check.groups()[0]
            proposal_text = "<custodianhash>"
            proposal_text += proposal_text_check
            proposal_text += "</custodianhash>"
            calculated_hash = hashlib.new('ripemd160', proposal_text).hexdigest()
            #we get to here we can update the data object
            match = True
            if original_hash != calculated_hash:
                match = False
            data.append({'type': 'custodial',
                         'original_hash': original_hash,
                         'calculated_hash': calculated_hash,
                         'match': match,
                         'link': "%s/t/%s/%s" % (bot.BaseUrl, topic_id, post_number)})
            continue

        if '<motionhash>' in post_text:
            bot.Log.info("found motion hash")
            original_hash = re.match(r'(.+)<calculatedmotionhash>(.+)', post_text, re.IGNORECASE | re.DOTALL)
            if original_hash is None:
                continue
            original_hash = re.match(r'(.+)</calculatedmotionhash>(.+)', original_hash.groups()[1], re.IGNORECASE | re.DOTALL)
            if original_hash is None:
                continue
            original_hash = original_hash.groups()[0]

            proposal_text_check = re.match(r'(.+)<motionhash>(.+)', post_text, re.IGNORECASE | re.DOTALL)
            if proposal_text_check is None:
                continue
            proposal_text_check = re.match(r'(.+)</motionhash>(.+)', proposal_text_check.groups()[1], re.IGNORECASE | re.DOTALL)
            if proposal_text_check is None:
                continue
            proposal_text_check = proposal_text_check.groups()[0]
            proposal_text = "<motionhash>"
            proposal_text += proposal_text_check
            proposal_text += "</motionhash>"
            calculated_hash = hashlib.new('ripemd160', proposal_text).hexdigest()
            #we get to here we can update the data object
            match = True
            if original_hash != calculated_hash:
                match = False
            data.append({'type': 'motion',
                         'original_hash': original_hash,
                         'calculated_hash': calculated_hash,
                         'match': match,
                         'link': "%s/t/%s/%s" % (bot.BaseUrl, topic_id, post_number)})
            continue

    custodial_count = 0
    motion_count = 0
    output = ""
    for hash in data:
        if hash['type'] == 'custodial':
            custodial_count += 1
        else:
            motion_count += 1
        output += "<p><strong><a href=\"%s\">%s</a> hash</strong><br>" % (hash['link'], hash['type'])
        output += "Original Hash   : <strong>%s</strong><br>" % hash['original_hash']
        output += "Calculated Hash : <strong>%s</strong><br>" % hash['calculated_hash']
        if hash['match'] is True:
            output += "The hash is good.</p>"
        else:
            output += "The hash has changed indicating a change to the text.</p>"


    reply = "Hi @%s\n\n" % mention.username
    reply += "Here are the verification results of all the motion and custodian hashes I could find in this thread:\n\n"
    reply += "I found the following hashes:\n<strong>%d Motion</strong> | <strong>%s Custodial</strong>\n\n" % (motion_count, custodial_count)
    reply += output
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def qr_code(bot, mention, address):
    if nud.validate_address(address) is False:
        reply = "Hi @%s\n\n" \
                "I'm afraid your request for a QR-Code failed as %s isn't a valid NBT address" % address
        bot.send_pm(mention.username, reply)
    img = qrcode.make(address)
    with open('conf.json') as conf_file:
        conf = json.loads(conf_file.read())
    img.save("%s/%s.png" % (conf['web_root'], address))
    url = "%s/%s.png" % (conf['web_front'], address)
    reply = "Hi @%s\n\n" \
            "Here is the QR Code of %s:\n" \
            "%s" % (mention.username, address, url)
    bot.reply_to(mention.topic_id, mention.post_number, reply)
