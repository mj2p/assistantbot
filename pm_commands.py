__author__ = 'woolly_sammoth'

import hashlib
import mistune
import json
from rpc import RPC
nud = RPC()


def motion_hash(bot, mention, data):
    """
    Reformat the given data into a formal motion complete with Ripemd160 hash
    """
    motion = data.groups()[0].encode('ascii', 'xmlcharrefreplace')

    hash_motion = "<motionhash><p><code>=##=##=##=##=##=## Motion hash starts with this line ##=##=##=##=##=##=</code></p>"
    hash_motion += "<p><motiontext>" + mistune.markdown(motion.strip()).replace("\n", "") + "</motiontext></p>"
    hash_motion += "<p><code>=##=##=##=##=##=## Motion hash ends with this line ##=##=##=##=##=##=</code></p></motionhash>"

    calculated_hash = hashlib.new('ripemd160', hash_motion).hexdigest()
    bot.Log.info("hash: %s" % calculated_hash)

    with open('/home/assistant/assistant/conf.json') as conf_file:
        conf = json.loads(conf_file.read())

    url = "%s/%s.txt" % (conf['web_front'], calculated_hash)

    tag_line = "<p><h5><a href=\"%s\">Verify.</a>" % url
    tag_line += " Use everything between and including the &lt;motionhash&gt;&lt;/motionhash&gt; tags.</h5></p>"

    reply = "Motion RIPEMD160 hash: **<calculatedmotionhash>%s</calculatedmotionhash>**\n\n%s" % (calculated_hash, hash_motion + "\n\n" + tag_line)

    with open("%s/%s.txt" % (conf['web_root'], calculated_hash), "w+") as hash_file:
        hash_file.write(reply)

    bot.reply_to(mention.topic_id, mention.post_number, reply)


def custodian_hash(bot, mention, address, amount, proposal):
    """
    Reformat the given data into a formal motion complete with Ripemd160 hash
    """
    proposal = proposal.encode('ascii', 'xmlcharrefreplace')

    hash_proposal = "<custodianhash><p><code>=##=##=##=##=##=## Custodian Hash starts with this line ##=##=##=##=##=##=</code></p>"
    hash_proposal += "<p><blockquote><p>Custodial Address: <strong><custodialaddress>" + address + "</custodialaddress></strong><br>"
    hash_proposal += "Amount Requested: <strong><custodialamount>" + amount + "</custodialamount> NBT</strong></p></blockquote></p>"
    hash_proposal += "<p><custodialproposal>" + mistune.markdown(proposal.strip()).replace("\n", "") + "</custodialproposal></p>"
    hash_proposal += "<p><code>=##=##=##=##=##=## Custodian Hash ends with this line ##=##=##=##=##=##=</code></p></custodianhash>"

    with open('/home/assistant/assistant/conf.json') as conf_file:
        conf = json.loads(conf_file.read())

    calculated_hash = hashlib.new('ripemd160', hash_proposal).hexdigest()
    bot.Log.info("hash: %s" % calculated_hash)

    url = "%s/%s.txt" % (conf['web_front'], calculated_hash)

    tag_line = "<p><h5><a href=\"%s\">Verify.</a>" % url
    tag_line += " Use everything between and including the &lt;custodianhash&gt;&lt;/custodianhash&gt; tags.</h5></p>"

    reply = "Proposal RIPEMD160 hash: **<calculatedcustodialhash>%s</calculatedcustodialhash>**\n\n%s" % (calculated_hash, hash_proposal + "\n\n" + tag_line)

    with open("%s/%s.txt" % (conf['web_root'], calculated_hash), "w+") as hash_file:
        hash_file.write(reply)

    bot.reply_to(mention.topic_id, mention.post_number, reply)


def invalid_hash(bot, mention):
    bot.Log.error("Problem getting json for post")
    reply = "Hi @%s\n\n" \
            "I'm afraid your hash request failed.\n" \
            "give @%s a poke to get it fixed" % (mention.username, bot.BotAdmin)
    if bot.send_pm(reply, mention.username) is False:
        reply += "\n\n**Please send a Private Message to @assistant in order to receive future notification by PM**"
        bot.reply_to(mention.topic_id, mention.post_number, reply)


def not_enough_balance(bot, mention, balance, amount, to_user):
    bot.Log.error("not enough balance for %s tip (%s NBT)" % (amount,balance))
    reply = "Hi @%s\n\n" \
            "I'm afraid your tip of %s NBT to %s failed as you don't have enough NuBits.\n\n" \
            "Your current balance is %s NBT" % (mention.username, amount, to_user, balance)
    if bot.send_pm(reply, mention.username) is False:
        reply += "\n\n**Please send a Private Message to @assistant in order to receive future notification by PM**"
        bot.reply_to(mention.topic_id, mention.post_number, reply)


def invalid_amount(bot, mention, amount, to_user):
    bot.Log.error("didn't recognise amount %s" % amount)
    reply = "Hi @%s\n\n" \
            "I'm afraid your tip of %s NBT to %s failed as I didn't recognise %s as a valid tip amount.\n" \
            "Please try again." % (mention.username, amount, to_user, amount)
    if bot.send_pm(reply, mention.username) is False:
        reply += "\n\n**Please send a Private Message to @assistant in order to receive future notification by PM**"
        bot.reply_to(mention.topic_id, mention.post_number, reply)


def invalid_user(bot, mention, amount, to_user):
    bot.Log.error("invalid to_user %s" % to_user)
    reply = "Hi @%s\n\n" \
            "I'm afraid your tip of %s NBT to %s failed " \
            "as %s isn't a registered user at %s" % (mention.username, amount, to_user, to_user, bot.BaseUrl)
    if bot.send_pm(reply, mention.username) is False:
        reply += "\n\n**Please send a Private Message to @assistant in order to receive future notification by PM**"
        bot.reply_to(mention.topic_id, mention.post_number, reply)


def tip_error(bot, mention, to_user, reason):
    bot.Log.error("tip error\nto: %s\nreason: %s" % (to_user, reason))
    reply = "Hi @%s\n\n" \
            "I'm afraid your tip to %s failed " \
            "The reason given was: %s" % (mention.username, to_user, reason)
    if bot.send_pm(reply, mention.username) is False:
        reply += "\n\n**Please send a Private Message to @assistant in order to receive future notification by PM**"
        bot.reply_to(mention.topic_id, mention.post_number, reply)
