__author__ = 'woolly_sammoth'

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import json
import os

class RPC:

    def __init__(self):
        # set up connection to nud
        rpc_user = None
        rpc_password = None
        self.encryption_password = None
        # get the user and password from the conf.json file
        with open(os.path.join(os.getcwd(), 'conf.json')) as conf_file:
            conf = json.loads(conf_file.read())
            rpc_user = conf['rpc_user']
            rpc_password = conf['rpc_password']
            self.encryption_password = conf['encryption_password']
        if rpc_user is None or rpc_password is None or self.encryption_password is None:
            raise Exception("Failed to load rpc conf")
        self.rpc = AuthServiceProxy("http://" + rpc_user + ":" + rpc_password + "@127.0.0.1:14002")

    def unlock(self):
        self.rpc.walletpassphrase(self.encryption_password, 30)
        pass

    def lock(self):
        self.rpc.walletlock()
        pass

    def list_accounts(self):
        try:
            self.unlock()
            accounts = self.rpc.listaccounts()
            self.lock()
        except JSONRPCException:
            return False
        return dict(accounts)

    def get_new_address(self, account):
        try:
            self.unlock()
            address = self.rpc.getnewaddress(account)
            self.lock()
        except JSONRPCException:
            return False
        return address

    def get_account_address(self, account):
        try:
            self.unlock()
            address = self.rpc.getaccountaddress(account)
            self.lock()
        except JSONRPCException:
            return False
        return address

    def get_balance(self, account):
        try:
            self.unlock()
            balance = self.rpc.getbalance(account)
            self.lock()
        except JSONRPCException:
            return False
        return balance

    def send_from(self, account, address, amount):
        try:
            self.unlock()
            tx = self.rpc.sendfrom(account, address, amount)
            self.lock()
        except JSONRPCException:
            return False
        return tx

    def move(self, from_account, to_account, amount):
        try:
            self.unlock()
            tx = self.rpc.move(from_account, to_account, amount)
            self.lock()
        except JSONRPCException:
            return False
        return tx

    def backup_wallet(self, destination):
        self.unlock()
        complete = self.rpc.backupwallet(destination)
        self.lock()
        return complete

    def get_block_count(self):
        try:
            self.unlock()
            count = self.rpc.getblockcount()
            self.lock()
        except JSONRPCException:
            return False
        return count

    def get_block_hash(self, index):
        try:
            self.unlock()
            block_hash = self.rpc.getblockhash(index)
            self.lock()
        except JSONRPCException:
            return False
        return block_hash

    def get_custodian_votes(self):
        try:
            self.unlock()
            votes = self.rpc.getcustodianvotes()
            self.lock()
        except JSONRPCException:
            return False
        return dict(votes)

    def get_elected_custodians(self):
        try:
            self.unlock()
            elected = self.rpc.getelectedcustodians()
            self.lock()
        except JSONRPCException:
            return False
        return list(elected)

    def get_difficulty(self):
        try:
            self.unlock()
            difficulty = self.rpc.getdifficulty()
            self.lock()
        except JSONRPCException:
            return False
        return dict(difficulty)

    def get_liquidity_info(self):
        try:
            self.unlock()
            liquidity = self.rpc.getliquidityinfo('B')
            self.lock()
        except JSONRPCException:
            return False
        return dict(liquidity)

    def get_motions(self):
        try:
            self.unlock()
            motions = self.rpc.getmotions()
            self.lock()
        except JSONRPCException:
            return False
        return dict(motions)

    def get_park_rates(self):
        try:
            self.unlock()
            rates = self.rpc.getparkrates()
            self.lock()
        except JSONRPCException:
            return False
        return dict(rates)

    def get_park_votes(self):
        try:
            self.unlock()
            votes = self.rpc.getparkvotes()
            self.lock()
        except JSONRPCException:
            return False
        return dict(votes)

    def get_transaction(self, txid):
        try:
            self.unlock()
            tx = self.rpc.gettransaction(txid)
            self.lock()
        except JSONRPCException:
            return False
        return tx

    def validate_address(self, address):
        try:
            self.unlock()
            valid = self.rpc.validateaddress(address)
            self.lock()
        except JSONRPCException:
            return False
        return dict(valid)

    def list_transactions(self, account, count=30):
        try:
            self.unlock()
            tx = self.rpc.listtransactions(account, count)
            self.lock()
        except JSONRPCException:
            return False
        return list(tx)




