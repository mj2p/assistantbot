__author__ = 'woolly_sammoth'

from decimal import Decimal
import json
import pm_commands
import utils
from rpc import RPC
nud = RPC()


def register(bot, mention):
    """
    Register for tipping.
    this sets up the account on nud and gives the deposit address
    """
    address = nud.get_account_address(mention.username)
    if address is False:
        #no address found, get new address
        address = nud.get_new_address(mention.username)
        if address is False:
            reply = utils.tip_error(bot, mention, "registration")
            bot.reply_to(mention.topic_id, mention.post_number, reply)
            return
    info(bot, mention)


def info(bot, mention):
    """
    Send the account info to the user
    """
    address = nud.get_account_address(mention.username)
    balance = nud.get_balance(mention.username)
    bot.Log.info("Info details\naddress: %s\nbalance: %s" % (address, balance))
    if address is False or balance is False:
        reply = utils.tip_error(bot, mention, "info")
    else:
        reply = "Hi @%s.\n\n" \
                "Here's your info:\n\n" \
                "Your deposit address is **%s**\n" \
                "You currently have a balance of **%s NBT**" % (mention.username, address, balance)
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def tip(bot, mention, amount, to_user=None):
    #check that the amount is valid
    check_amount = utils.check_amount(amount)
    if check_amount is False:
        pm_commands.invalid_amount(bot, mention, amount, to_user)
        return
    else:
        amount = Decimal(check_amount)
    #check if to_user is a valid user, otherwise get the author of the parent or reply to post
    check_to_user = utils.check_user(bot, mention, to_user)
    if check_to_user is False:
        pm_commands.invalid_user(bot, mention, amount, to_user)
        return
    else:
        to_user = check_to_user
    #check the users balance - create an account if they don't have one
    address = nud.get_account_address(mention.username)
    if address is False:
        pm_commands.tip_error(bot, mention, to_user, "Error getting your address")
        return
    balance = nud.get_balance(mention.username)
    if balance is False:
        pm_commands.tip_error(bot, mention, to_user, "Error getting your balance")
        return
    elif Decimal(balance) < Decimal(amount):
        pm_commands.not_enough_balance(bot, mention, balance, amount, to_user)
        return

    #we get here, we're good to go
    #check if the to_user has an account and create one if necessary
    nud.get_account_address(to_user)
    if bot.TestMode is False:
        #to_address = nud.get_account_address(to_user)
        #nud.send_from(mention.username, to_address, amount)
        move = nud.move(mention.username, to_user, amount)
        if move is False:
            reply = utils.tip_error(bot, mention, "Tip", "Movement of funds failed")
        else:
            reply = "Hi @%s\n\nYour tip of %s NBT has been credited to @%s" % (mention.username, amount, to_user)
    else:
        reply = "@assistant is running in **test mode**\n\nTip of %s NBT requested from @%s to @%s but not completed due to **test mode**" % (amount, mention.username, to_user)
    bot.Log.info("tip details\namount: %s\nfrom: %s\nto: %s" % (amount, mention.username, to_user))
    if bot.send_pm(reply, mention.username) is False:
        reply += "\n\n**Please send a Private Message to @assistant in order to receive future notification by PM**"
        bot.reply_to(mention.topic_id, mention.post_number, reply)
    to_user_message = "Hi @%s\n\nYou were just sent a tip of %s NBT by %s" % (to_user, amount, mention.username)
    if bot.send_pm(to_user_message, to_user) is False:
        to_user_message += "\n\n**Please send a Private Message to @assistant with the first word 'register' to receive the tip and view your details**"
        bot.reply_to(mention.topic_id, mention.post_number, to_user_message)
    with open('conf.json') as conf_file:
        conf = json.loads(conf_file.read())
        if conf['tip_badge_id'] != "":
            bot.grant_badge(to_user, conf['tip_badge_id'])
        if conf['tip_success_badge_id'] != "":
            bot.grant_badge(mention.username, conf['tip_success_badge_id'])


def history(bot, mention):
    address = nud.get_account_address(mention.username)
    txs = nud.list_transactions(mention.username)
    bot.Log.info("history details\naddress: %s\ntxs: %s" % (address, txs))
    if address is False or txs is False:
        reply = utils.nud_error(bot, mention, 'transactions')
    else:
        output = "----------\n"
        reply = "Hi @%s\n\n" \
                "Here are your latest transactions:\n\n" % mention.username
        for tx in txs:
            tx_type = "tip" if tx['category'] == 'move' else tx['category']
            output += "Time: %d\n" % tx['time']
            output += "Type: %s\n" % tx_type
            output += "Amount: %f NBT\n" % tx['amount']
            if tx_type == "send" or tx_type == "receive":
                output += "To: %s\n" % tx['address']
                output += "txid: %s\n" % tx['txid']
            else:
                output += "To: %s\n" % tx['otheraccount']
            reply += "%s\n" % output
            output = "----------\n"
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def withdraw(bot, mention, data):
    param = data.groups()[0].split()
    to_address = nud.validate_address(param[0])
    if to_address is False or to_address['isvalid'] is False:
        reply = utils.nud_error(bot, mention, 'withdraw')
    else:
        if to_address['isvalid'] is False:
            reply = "Hi @%s\n\n" \
                    "I'm afraid the NBT address you supplied is invalid" % mention.username
        else:
            to_address = to_address['address']
            address = nud.get_account_address(mention.username)
            balance = nud.get_balance(mention.username)
            if address is False or balance is False:
                reply = utils.nud_error(bot, mention, 'withdraw')
            else:
                tx = nud.send_from(mention.username, to_address, (Decimal(balance) - Decimal(0.01)))
                if tx is False:
                    reply = utils.nud_error(bot, mention, 'withdraw')
                else:
                    reply = "Hi @%s\n\n" \
                            "Your balance of %s NBT was successfully sent to %s\n\n" \
                            "The txid is %s" % (mention.username, balance, to_address, tx)
                    bot.Log.info("withdraw details\nuser: %s\nbalance: %s\nto address: %s" % (mention.username, balance, to_address))
    bot.reply_to(mention.topic_id, mention.post_number, reply)


def keywords(bot, mention):
    reply = "Hi @%s\n\n" \
            "Here is a list of accepted tipping keywords:\n\n" % mention.username
    with open('amounts.json') as amounts_file:
        amounts = json.loads(amounts_file.read())
        for amount in amounts:
            reply += "%s = %f NBT\n" % (amount.lower(), amounts[amount])
    bot.reply_to(mention.topic_id, mention.post_number, reply)
