__author__ = 'woolly_sammoth'
from decimal import Decimal
from email.mime.text import MIMEText
import json
import requests
import smtplib
import traceback
import os


def notify(message=None):
    # Construct MIME message
    with open(os.path.join(os.getcwd(), 'conf.json')) as conf_file:
        conf = json.loads(conf_file.read())
        email_subject = conf['email_subject']
        from_address = conf['from_address']
        to_address = conf['to_address']
        smtp_server = conf['smtp_server']
        smtp_password = conf['smtp_password']
    msg = MIMEText(str(message) + "\n\n" + str(traceback.format_exc()))
    msg['Subject'] = email_subject
    msg['From'] = from_address
    msg['To'] = to_address

    # Send MIME message
    server = smtplib.SMTP(smtp_server)
    server.starttls()
    server.login(to_address, smtp_password)
    server.sendmail(from_address, to_address, msg.as_string())
    server.quit()
    return


def nud_error(bot, mention, request):
    reply = "Hi @%s\n\n" \
            "I'm afraid there was an issue with your %s request.\n\n" \
            "Give @%s a poke and get to fix it." % (mention.username, request, bot.BotAdmin)
    bot.Log.error("nud error - %s" % request)
    return reply


def tip_error(bot, mention, request, reason):
    reply = "Hi @%s\n\n" \
            "I'm afraid there was an error when dealing with your %s request.\n\n" \
            "The reason given was: %s" \
            "Give @%s a poke to get it fixed" % (mention.username, request, reason, bot.BotAdmin)
    bot.Log.error("tip error - %s" % request)
    return reply


def hash_error(bot, mention, request):
    reply = "Hi @%s\n\n" \
            "I'm afraid there was an error when dealing with your %s request.\n\n" \
            "Give @%s a poke to get it fixed" % (mention.username, request, bot.BotAdmin)
    bot.Log.error("hash error - %s" % request)
    return reply


def format_search(results, type):
    score = Decimal(0)
    out = False
    topic_id = None
    for result in results['posts']:
        if type == "custodian" and "<code>=##=##=##=##=##=## Custodian Hash starts with this line ##=##=##=##=##=##=</code>" not in result['cooked']:
            continue
        if type == "motion" and "<code>=##=##=##=##=##=## Motion hash starts with this line ##=##=##=##=##=##=</code>" not in result['cooked']:
            continue
        if Decimal(result['score']) > score:
            out = {'slug': result['topic_slug'], 'cooked': result['cooked']}
            score = Decimal(result['score'])
            topic_id = result['topic_id']
    if topic_id is not None:
        for topic in results['topics']:
            if topic['id'] == topic_id:
                out['title'] = topic['fancy_title']
    return out


def check_amount(amount):
    amount = amount.replace(",", "")
    try:
        Decimal(amount)
    except Exception:
        #see if it's a keyword
        with open('amounts.json') as amounts_file:
            amounts = json.loads(amounts_file.read())
            try:
                amount = amounts[amount]
            except KeyError:
                return False
    return amount


def get_author(bot, mention):
    #get the list of posts and start popping
    posts = get_posts(bot, mention.topic_id, mention.post_number)
    post = posts.pop()
    #until we find the post that matches our mention
    while str(post['post_number']) != str(mention.post_number):
        post = posts.pop()
    #find out if the mention was a reply
    reply_to = post['reply_to_post_number']
    if reply_to is None:
        #if it wasn't a reply, get the next post in the stream
        post = posts.pop()
    else:
        #get the stream for the reply to post
        r = requests.get("%s/t/%s/%s.json" % (bot.BaseUrl, mention.topic_id, reply_to))
        if r.status_code != requests.codes.ok:
            bot.Log.error("couldn't get post data from %s" % r.url)
            return False
        try:
            data = r.json()
        except ValueError:
            bot.Log.error("couldn't get post data from %s" % r.url)
            return False
        posts = list(data['post_stream']['posts'])
        post = posts.pop()
        while post['post_number'] != reply_to:
            post = posts.pop()
    #last check to avoid tipping yourself
    while post['username'] == mention.username:
        post = posts.pop()
    return post['username']


def check_user(bot, mention, to_user):
    was_user = False
    if to_user is not None:
        if to_user.startswith('@'):
            was_user = True
            to_user = to_user.replace("@", "")
        r = requests.get("%s/users/%s.json" % (bot.BaseUrl, to_user))
        if r.status_code == requests.codes.not_found:
            #tip was intended for user but user couldn't be found
            if was_user is True:
                return False
            #otherwise get the parent_post or reply_to author
            to_user = get_author(bot, mention)
    else:
        to_user = get_author(bot, mention)
    if to_user is False:
        return False
    return to_user


def get_posts(bot, topic_id, post_number):
    data = bot.get_posts(topic_id, post_number)
    return list(data['post_stream']['posts'])
